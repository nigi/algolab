#include <iostream>
#include <vector>

using namespace std;

// Forward Definitions
void colorComponent(int currentVertex, vector<bool>& marked, 
		    vector<vector<int> >& graph);

int main()
{
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);

    int n,m; // number of nodes and edges for each specific case

    while(cin >> n >> m)
    {
	// cout << "New case with " << n << " nodes and " << m << " edges\n";
	int n1,n2; // nodes one and two of edge
		
	vector<vector<int> > nodes(n);
		
	for(int i = 0; i < n; i++)
	{
	    nodes[i] = vector<int>();
	}
		
	for(int i = 0; i < m; i++)
	{
	    cin >> n1 >> n2;
			
	    nodes[n1-1].push_back(n2-1);
	    nodes[n2-1].push_back(n1-1);
			
	    // cout << "New edge from " << n1 << " to " << n2 << "\n";	
	}
		
	// Traversing graph for number of components
	vector<bool> marked(n);
	int k = 0;	// number of components
		
	// Go through all vertices and if they are not colored already
	// color whole subgraph and add one to the number of components
	for(int vertex = 0; vertex < n; vertex++)
	{
	    if(!marked[vertex])
	    {
		k++;
		colorComponent(vertex,marked,nodes);
	    }
	}
		
	// cout << "Components: " << k << "\n";
	// Compute number of faces with eulers formular
	int f = 1 + k + m - n;
	cout << f << "\n";
    }
	
    return 0;
}

void colorComponent(int currentVertex, vector<bool>& marked, 
		    vector<vector<int> >& graph)
{
    marked[currentVertex] = true;

    for(int i = 0; i < graph[currentVertex].size(); i++)
    {
	if(!marked[graph[currentVertex][i]])
	{
	    colorComponent(graph[currentVertex][i], marked, graph);
	}	
    }
} 
