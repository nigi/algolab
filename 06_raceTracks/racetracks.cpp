#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <queue>
#include <utility>

using namespace std;

struct vec2 {
    int x;
    int y;

    vec2(){
	x = 0;
	y = 0;
    }

    vec2(int a, int b){
	x = a;
	y = b;
    }

    vec2 operator+(vec2 b){
	return vec2(b.x+x,b.y+y);
    }

    bool operator==(vec2 b){
	return x == b.x && y == b.y;
    }
};

struct Node {
    int hops;
    vec2 p;
    vec2 v;

    Node(vec2 position, vec2 velocity, int jumps){
	assert(jumps >= 0);

	p = position;
	v = velocity;
	hops = jumps;
    }

};

struct Grid {
    int width;
    int height;

    vector<vector<pair<bool,vector<bool> > > > squares;
    
    Grid(int w, int h){
	assert(w >= 1 && w <= 30);
	assert(h >= 1 && h <= 30);

	width = w;
	height = h;

	for(int i = 0; i < w; i++){
	    squares.push_back(vector<pair<bool,vector<bool> > > (h));
	}
    }

    int velocityHash(vec2 v){
	assert(v.x >= -3 && v.x <= 3);
	assert(v.y >= -3 && v.y <= 3);

	int vhash = (v.x+3) + 7 * (v.y+3);
	assert(vhash >= 0 && vhash < 49);

	return vhash;
    }
    
    bool isOccupied(int x, int y){
	assert(x >= 0 && x < width);
	assert(y >= 0 && y < height);

	return squares[x][y].first;
    }

    bool hasBeenVisited(Node & n){
	int vhash = velocityHash(n.v);
	return squares[n.p.x][n.p.y].second[vhash];
    }

    void insertObstacle(vec2 upperLeft, vec2 lowerRight){
	for(int x = upperLeft.x; x <= lowerRight.x; x++){
	    for(int y = upperLeft.y; y <= lowerRight.y; y++){
		squares[x][y].first = true;
	    }
	}
    }

    void visit(Node & n){
	assert(n.p.x >= 0 && n.p.x < width);
	assert(n.p.y >= 0 && n.p.y < height);

	squares[n.p.x][n.p.y].second[velocityHash(n.v)] = true;
    }

    void pushNeighbors(Node & n, queue<Node> & nodes){
	vec2 newpos = n.p + n.v;
	
	vec2 delta(-1,-1);
	for(delta.x = -1; delta.x <=1; delta.x++){
	    for(delta.y = -1; delta.y<=1; delta.y++){
		Node t(newpos + delta, n.v + delta, n.hops+1);
		if(isValidNode(t)){
		    visit(t);
		    nodes.push(t);
		}
	    }
	}
    }

    bool isValidNode(Node & n){
	bool valid = true;
	valid &= (n.v.x >= -3 && n.v.x <= 3); // valid velocity x
	valid &= (n.v.y >= -3 && n.v.y <= 3); // valid velocity y
	valid &= (n.p.x >= 0 && n.p.x < width); // valid x position
	valid &= (n.p.y >= 0 && n.p.y < height); // valid x position

	if(!valid) return valid;

	valid &= !isOccupied(n.p.x,n.p.y); // not occupied
	valid &= !hasBeenVisited(n); // not visited
	return valid;
    }
};

void processTestCase();
int minimalNumberOfHops(vec2 start, vec2 end, Grid & grid);

int main(){
    // Read in initial data
    int N; // Number of testcases
    cin >> N;

    while(N-- > 0){
	processTestCase();
    }
}

void processTestCase(){
    
    int X,Y; // width and height of the grid
    cin >> X >> Y;

    assert(X >= 1 && X <= 30);
    assert(Y >= 1 && Y <= 30);

    vec2 startP, endP; // start and end position

    cin >> startP.x >> startP.y >> endP.x >> endP.y;

    assert(startP.x >= 0 && startP.x < X);
    assert(endP.x >= 0 && endP.x < X);
    assert(startP.y >= 0 && startP.y < Y);
    assert(endP.y >= 0 && endP.y < Y);

    int P; // number of obstacles
    cin >> P;

    assert(P >= 0 && P <= 10);

    Grid grid(X,Y);

    while(P-- > 0){
	vec2 upperLeft, lowerRight;
	cin >> upperLeft.x >> upperLeft.y >> lowerRight.x >> lowerRight.y;

       	assert( upperLeft.x >= 0 && upperLeft.y >= 0);
	assert( lowerRight.x >= 0 && lowerRight.y >= 0);
	assert( lowerRight.x < X && lowerRight.y < Y);
	assert( upperLeft.x < X && upperLeft.y < Y);
	
	grid.insertObstacle(upperLeft,lowerRight);
    }

    int hops = minimalNumberOfHops(startP,endP,grid);

    if(hops < 0){
	cout << "No solution.\n";
    }else{
	cout << "Optimal solution takes " << hops << " hops.\n";
    }
}

int minimalNumberOfHops(vec2 startP, vec2 endP, Grid & grid)
{
    Node start(startP,vec2(0,0),0);
    

    queue<Node> nodes;

    if(grid.isValidNode(start)){
	grid.visit(start);
	nodes.push(start);
    }else{
	return -1;
    }

    bool solutionFound = false;
    int minHops = 0;

    while(nodes.size() > 0){
	Node n = nodes.front();
	nodes.pop();

	if(n.p == endP && (!solutionFound || n.hops < minHops)){
	    solutionFound = true;
	    minHops = n.hops;
	    break;
	}else{
	    grid.pushNeighbors(n,nodes);
	}
    }

    return solutionFound ? minHops : -1;
}
