#include <iostream>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>



using namespace std;

void solveProblem(int p, int a, int b);

int main(){
    while(true){
	int p; cin >> p;
	if(p == 0) break;

	int a,b;
	cin >> a >> b;
	solveProblem(p,a,b);
    }
}

// choose the type
#ifdef CGAL_USE_GMP
#include <CGAL/Gmpz.h>
typedef CGAL::Gmpz ET;
#else
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;
#endif

// program and solution types
typedef CGAL::Quadratic_program<int> Program; // int is the input type!!
typedef CGAL::Quadratic_program_solution<ET> Solution;

// round down to next int (the result will be small enough)
int ceil_to_int(const CGAL::Quotient<ET>& x) {
  int a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}

void solveProblem(int p, int a, int b)
{
    const int X = 0;
    const int Y = 1;
    const int ZZ = 2;

    if(p == 1){
	Program lp(CGAL::SMALLER,true,0,false,0);
	
	lp.set_a(X,0,1);  lp.set_a(Y,0,1); lp.set_b(0,4);
	lp.set_a(X,1,4);  lp.set_a(Y,1,2); lp.set_b(1,a*b);
	lp.set_a(X,2,-1); lp.set_a(Y,2,1); lp.set_b(2,1);

	lp.set_c(Y,-b);
	lp.set_d(X,X,2*a); 

	Solution s = CGAL::solve_nonnegative_quadratic_program(lp,ET());

	if(s.is_optimal()){
	    cout << -1*ceil_to_int(s.objective_value()) << "\n";
	}else if(s.is_infeasible()){
	    cout << "no\n";
	}else if(s.is_unbounded()){
	    cout << "unbounded\n";
	}

    }else if(p == 2){
	Program lp(CGAL::LARGER,false,0,true,0);
	
	lp.set_a(X,0,1);  lp.set_a(Y,0,1); lp.set_a(ZZ,0,0); lp.set_b(0,-4);
	lp.set_a(X,1,4);  lp.set_a(Y,1,2); lp.set_a(ZZ,1,1); lp.set_b(1,-a*b);
	lp.set_a(X,2,-1); lp.set_a(Y,2,1); lp.set_a(ZZ,2,0); lp.set_b(2,-1);

	lp.set_c(Y,b);
	lp.set_d(X,X,2*a); 
	lp.set_d(ZZ,ZZ,2);

	Solution s = CGAL::solve_quadratic_program(lp,ET());

	if(s.is_optimal()){
	    cout << ceil_to_int(s.objective_value()) << "\n";
	}else if(s.is_infeasible()){
	    cout << "no\n";
	}else if(s.is_unbounded()){
	    cout << "unbounded\n";
	}
    }
}
