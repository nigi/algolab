#include <iostream>

using namespace std;

int main()
{
	int n; // number of testcases
		
	cin >> n;
	
	for(int i = 0; i < n; i++)
	{
		int m; // number of numbers in testcase
		cin >> m;
		
		float sum = 0;
		float f = 0;
		for(int j = 0; j < m; j++)
		{	
			cin >> f;
			sum+=f;
		}
		
		cout << sum << "\n";
			
	}
	
	return 0;
}