#include <iostream>
#include <cassert>

#include <CGAL/basic.h>
#include <CGAL/QP_models.h>
#include <CGAL/QP_functions.h>

// choose the type
#ifdef CGAL_USE_GMP
#include <CGAL/Gmpz.h>
typedef CGAL::Gmpz ET;
#else
#include <CGAL/MP_Float.h>
typedef CGAL::MP_Float ET;
#endif

using namespace std;

void processTestCase(int n, int m);
double floor_to_int(const CGAL::Quotient<ET>& x);

int main(){
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);

    while(true){
	int n,m;
	cin >> n >> m;
	if(n == 0 && m == 0) break;
	assert(n >= 1 && n <= 40);
	assert(m >= 1 && m <= 100);
	processTestCase(n,m);
    }
}

// program and solution types
typedef CGAL::Quadratic_program<double> Program; // double is the input type!!
typedef CGAL::Quadratic_program_solution<ET> Solution;

// Compute the minimal cost Sum(w_j,p_j)
// with the constraints.
//   n ... number of nutritiens = number of constraints
//   m ... number of foods = number of variables
void processTestCase(int n, int m){
    Program lp(CGAL::SMALLER,true,0,false,0);

    for(int in = 0; in < n; in++){
	// Read a nutritien and create the corresponding constraints
	double min_i, max_i;
	cin >> min_i >> max_i;
	
	// First min then max
	lp.set_b(2*in, min_i); lp.set_r(2*in, CGAL::LARGER);
	lp.set_b(2*in+1, max_i);
    }

    for(int im = 0; im < m; im++){
	double cost; cin >> cost;
	lp.set_c(im,cost);

	for(int in = 0; in < n; in++){
	    double amount_ij; cin >> amount_ij;
	    lp.set_a(im,2*in,amount_ij);
	    lp.set_a(im,2*in+1,amount_ij);
	}
    }

    // Solve this linear program
    
    Solution s = CGAL::solve_nonnegative_linear_program(lp,ET());

    if(s.is_optimal()){
	cout << floor_to_int(s.objective_value()) << "\n";
    }else if(s.is_infeasible()){
	cout << "No such diet.\n";
    }else if(s.is_unbounded()){
	assert(false);
    }
}



// round down to next double
double floor_to_int(const CGAL::Quotient<ET>& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}
