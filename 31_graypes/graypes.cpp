#include <iostream>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel_with_sqrt.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;
using namespace CGAL;

typedef Exact_predicates_inexact_constructions_kernel K;
typedef Exact_predicates_exact_constructions_kernel_with_sqrt::FT ExactFT;

typedef Delaunay_triangulation_2<K> Triangulation;

// round up to next integer double
double ceil_to_double(const ExactFT& x)
{
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}

void processTestCase(long n){
    // read input
    vector<K::Point_2> points;
    points.reserve(n);
    double x,y;
    for(long in = 0; in < n; in++){
	cin >> x >> y;
	points.push_back(K::Point_2(x,y));
    }

    Triangulation triang;
    triang.insert(points.begin(),points.end());
    
    Triangulation::Finite_edges_iterator e = triang.finite_edges_begin();
    K::FT mindist = triang.segment(*e).squared_length();
    while(++e != triang.finite_edges_end()){
	mindist = std::min(mindist,triang.segment(*e).squared_length());
    }

    cout << ceil_to_double(50*sqrt(ExactFT(mindist))) << "\n";
}

int main(){
    // set precision as always
    cout << setiosflags(ios::fixed) << setprecision(0); 
    
    for(long n; cin >> n && n > 0;){
	processTestCase(n);
    }
}
