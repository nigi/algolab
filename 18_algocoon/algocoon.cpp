#include <iostream>
#include <cassert>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/one_bit_color_map.hpp> // For the parity map
#include <boost/property_map/property_map.hpp>
#include <boost/graph/stoer_wagner_min_cut.hpp>

using namespace std;
using namespace boost;


typedef struct {
     int v1;
     int v2;
     int cost;
} Connection; // defines an entanglement of two figures a and b with cutting cost c


void processTestCase();
int minCut(int n, int m, vector<Connection> & con_points, vector<int> & setA);

int main(){
    int t; // testcases
    cin >> t;
    assert(t >= 1 && t <= 100);
    while(t--){
	processTestCase();
    }
}

/*
 * As always read input an pass it to actual processing function.
 */
void processTestCase(){
    // Read in SCULPTURE
    int n,m; // n .. number of figures,
             // m .. number of places where two of the figures are entangled
    cin >> n >> m;
    assert(n >= 2 && n <= 100);
    assert(m >= 0 && m <= 10000);
    
    vector<Connection> connections;
    
    for(int im = 0; im < m; im++){
	int a,b,c;
	cin >> a >> b >> c;
	assert(a >= 0 && a <= n);
	assert(b >= 0 && b <= n);
	assert(c >= 1 && c <= 1000);
	assert(a != b);

	Connection con = {a,b,c};
	connections.push_back(con);
    }

    vector<int> setA;
    int minc = minCut(n,m,connections,setA);

    cout << minc << "\n";
    cout << setA.size() << " ";
    for(int i = 0; i < setA.size(); i++){
	cout << setA[i] << " ";
    }
    cout << "\n";
}

typedef adjacency_list<vecS,vecS,undirectedS,
		       no_property,
		       property<edge_weight_t,int> > Graph;

typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef property_map<Graph,edge_weight_t>::type WeightMap;


/* Computes the minimal cut through the figures
 * Returns -1 if the cut is not possible 
 *   the minimal cut weight otherwise
 */
int minCut(int n, int m, vector<Connection> & con_points, vector<int> & setA)
{
    Graph g(n);
    WeightMap weights = get(edge_weight,g);

    for(int im = 0; im < m; im++){
	Connection c = con_points[im];
	
	Edge e; bool s;
	tie(e,s) = add_edge(c.v1,c.v2,g);
	weights[e] = c.cost;
    }

    BOOST_AUTO(parityMap,make_one_bit_color_map(n,get(vertex_index,g)));

    int minCut = stoer_wagner_min_cut(g,weights,parity_map(parityMap));

    for(int i = 0; i < n; i++){
	if(get(parityMap,i))
	    setA.push_back(i);
    }
    
    return minCut;
}
