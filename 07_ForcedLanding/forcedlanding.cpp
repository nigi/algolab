#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

struct vec2 {
    int x;
    int y;

    vec2(){
	x = 0;
	y = 0;
    }

    vec2(int a, int b){
	x = a;
	y = b;
    }
};

// Forward definitions
void processTestCase();
bool findOptimalSolution(int w, int h, 
			 int s, int r,
			 vector<vec2> & trees,
			 vec2 & optimalStartPos);
bool checkSpace(vec2 startPos, int s, int r, vector<vector<int> > & prefixMatrix, int w, int h);


int main(){

    // Read input
    int N; // Number of testcases
    cin >> N; 

    while(N-- > 0){
	processTestCase();
    }
}

void processTestCase(){
    int w,h; // width and height of the area we could land in
    cin >> w >> h;

    assert( w >= 0 && w <= 100000);
    assert( h >= 0 && h <= 100000);    

    int s, // needed width for landing
	r, // runway length
	n; // number of trees
    cin >> s >> r >> n;

    vector<vec2> trees;

    while(n-- > 0){
	vec2 tree;
	cin >> tree.x >> tree.y;
	trees.push_back(tree);

	assert(tree.x >= 0 && tree.x < w);
	assert(tree.y >= 0 && tree.y < h);
    }

    vec2 optimalStartPos;
    if(findOptimalSolution(w,h,s,r,trees, optimalStartPos)){
	cout << optimalStartPos.x << " " << optimalStartPos.y << "\n";
    }else{
	cout << "Impossible\n"; 
    }
}


bool findOptimalSolution(int w, int h, 
			 int s, int r,
			 vector<vec2> & trees,
			 vec2 & optimalStartPos)
{

    if(s == 0 || r == 0){
	optimalStartPos = vec2(0,0);
	return true;
    }else if(w == 0 || h == 0){
	return false;
    }

    // cout << "w: " << w << " h: " << h << "\n";
    // build a w x h matrix containing the prefixsum of each row
    vector<vector <int> > prefixMatrix;

    for(int x = 0; x <= w; x++){
	prefixMatrix.push_back(vector<int>(h+1,0));
    }



    // insert all trees
    for(int t = 0; t < trees.size(); t++){
	assert(t < trees.size());
	vec2 tree = trees[t];
	assert(tree.x >= 0 && tree.x < prefixMatrix.size());
	assert(tree.y >= 0 && tree.y < h);
	prefixMatrix[tree.x][tree.y] = 1;
    }

    // compute the prefix sums
    for(int y = 0; y < h; y++){
	for(int x = 1; x < w; x++){
	    prefixMatrix[x][y] = prefixMatrix[x-1][y] + prefixMatrix[x][y];
	}
    }

    bool fits = false;    
    // go through all positions and check if there is enough space
    for(int x = 0; x < w && !fits; x++){
	for(int y = 0; y < h && !fits; y++){
	    vec2 pos(x,y);
	    if(checkSpace(pos, s,r,prefixMatrix, w , h)){
		fits = true;
		optimalStartPos = pos;
	    }
	}
    }

    //return false;
    return fits;
}

bool checkSpace(vec2 startPos, int s, int r, vector<vector<int> > & prefixMatrix, int w, int h)
{
    // s is height in y direction
    // r is width in x direction
    
    if(startPos.x + r-1 >= prefixMatrix.size() || startPos.y + s-1 >= h)
	return false;

    bool isFree = true;
    for(int y = startPos.y + 1; y<startPos.y+s; y++){
	assert(startPos.x >= 0 && startPos.x < prefixMatrix.size());
	assert(startPos.x + r-1 >= 0 && startPos.x + r-1 < prefixMatrix.size());
	assert(y >= 0 && y < h);

	if(prefixMatrix[startPos.x][y] != prefixMatrix[startPos.x+r-1][y]){
	    isFree = false;
	    break;
	}
    }

    return isFree;
}
