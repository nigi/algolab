#include <iostream>
#include <vector>
#include <cassert>

using namespace std;

struct Floor {
    int currentWeight;
    int capacity;
    int cost;
};

void processTestCase();
int minDemolitionCost(vector<Floor> & floors);

int main(){
    int t; // number of testcases
    cin >> t;

    assert(t <= 5);

    while(t--)
    {
	processTestCase();
    }
}


void processTestCase(){
    int n; // number of floors
    cin >> n;

    assert(n >= 1 && n <= 100000);

    vector<Floor> floors;
    while(n--){
	Floor f;
	cin >> f.currentWeight >> f.capacity >> f.cost;
	assert(f.currentWeight <= f.capacity);
	floors.push_back(f);
    }
    cout << minDemolitionCost(floors) << "\n";
}

int minDemolitionCost(vector<Floor> & floors){
    
    return 0;
}
