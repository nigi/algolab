#include <iostream>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>

using namespace std;
using namespace CGAL;

typedef Exact_predicates_inexact_constructions_kernel K;
typedef Delaunay_triangulation_2<K> Triangulation;


int main(){
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);
    
    cout << setiosflags(ios::fixed) << setprecision(0); 

    for(long n = 0; cin >> n && n > 0;){
	
	vector<K::Point_2> points;
	points.reserve(n);
	for(long in = 0; in < n; in++){
	    K::Point_2 p;
	    cin >> p;
	    points.push_back(p);
	}

	Triangulation t;
	t.insert(points.begin(),points.end());
	
	long m;
	cin >> m;
	for(long im = 0; im < m; im++){
	    K::Point_2 p;
	    cin >> p;
	    cout << to_double(squared_distance(t.nearest_vertex(p)->point(),p)) << "\n";
	}

    }
}
