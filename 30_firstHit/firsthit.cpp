#include <iostream>
#include <algorithm>

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/intersections.h>

using namespace std;
using namespace CGAL;

typedef Exact_predicates_exact_constructions_kernel K;

// round up to next integer double
double ceil_to_double(const K::FT& x)
{
  double a = std::ceil(CGAL::to_double(x));
  while (a < x) a += 1;
  while (a-1 >= x) a -= 1;
  return a;
}

// round down to next double
double floor_to_double(const K::FT& x)
{
  double a = std::floor(CGAL::to_double(x));
  while (a > x) a -= 1;
  while (a+1 <= x) a += 1;
  return a;
}

void resize_segment(K::Segment_2 & seg, Object o){
    if(const K::Point_2 * op = object_cast<K::Point_2>(&o)){
	seg = K::Segment_2(seg.source(),*op);
    }else if(const K::Segment_2 * os = object_cast<K::Segment_2>(&o)){
	// select closer endpoint of the segment
	if(collinear_are_ordered_along_line(seg.source(),os->source(),os->target())){
	    // the source is closer
	    seg = K::Segment_2(seg.source(),os->source());
	}else{
	    // the target is closer
	    seg = K::Segment_2(seg.source(),os->target());
	}
    }
}

int main(){
    std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0); 
      
    for(size_t n = 0; cin >> n && n > 0;){
	K::Ray_2 r;
	cin >> r;

	vector<K::Segment_2> segs;
	segs.reserve(n);

	// Read in all segments
	double ax,ay,bx,by;
	for(size_t in = 0; in < n; ++in){
	    // Read in as doubles since this is much faster
	    cin >> ax >> ay >> bx >> by;
	    segs.push_back(K::Segment_2(K::Point_2(ax,ay),K::Point_2(bx,by)));
	}

	// randomly shuffle to get nice ordering
	random_shuffle(segs.begin(),segs.end());

	// use segments for intersection tests and cut it down on each hit
	K::Segment_2 raySeg(r.source(),r.source());

	// find a first hit to initialize the segment
	size_t i = 0; // if we iterate through whole vector without a hit there is no hit
	for(;i<n;++i){
	    if(do_intersect(segs[i],r)){
		resize_segment(raySeg,intersection(segs[i],r));
		break;
	    }
	}

	if(i == n) {
	    cout << "no\n";
	    continue;
	}

	for(;i<n;i++){
	    if(do_intersect(segs[i],raySeg)){
		resize_segment(raySeg,intersection(segs[i],r));
	    }
	}
	    
	cout << floor_to_double(raySeg.target().x()) << " " << floor_to_double(raySeg.target().y()) << "\n";
	
    }

}

