#include <iostream>
#include <cassert>
#include <vector>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace std;
using namespace boost;

typedef pair<int,int> E;

void processTestCase();
int minimalTime(int t, int s, 
		int a, int b,
		vector<E> & edges,
		vector<vector<int> > & weights,
		vector<int> & hives);

int main(){
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);

    int n; // number of testcases
    cin >> n;
    while(n--){
	processTestCase();
    }
}

void processTestCase(){
    // t ... number of trees 1 <= t <= 100
    // e ... number of edges
    // s ... number of species 1 <= s <= 10
    // a,b ... zero based indices of start and end tree
    int t,e,s,a,b;
    cin >> t >> e >> s >> a >> b;
    assert(t >= 1 && t <= 100);
    assert(s >= 1 && s <= 10);
    assert(a >= 0 && a < t);
    assert(b >= 0 && b < t);

    vector<E> edges;
    vector<vector<int> >weights(s);

    for(int ei = 0; ei < e; ei++){
	E etmp;
	cin >> etmp.first >> etmp.second;
	assert(etmp.first >= 0 && etmp.first < t);
	assert(etmp.second >= 0 && etmp.second < t); 
	edges.push_back(etmp);

	for(int i = 0; i < s; i++){
	    int w;
	    cin >> w;
	    weights[i].push_back(w);
	}
    }

    vector<int> hives;

    for(int si = 0; si < s; si++){
	int h;
	cin >> h;
	assert(h >= 0 && h < t);
	hives.push_back(h);
    }
    
    // Compute the minimal time for the testcase
    int time = minimalTime(t,s,a,b,edges,weights,hives);
    cout << time << "\n";
}

typedef adjacency_list<vecS,vecS,undirectedS,no_property,property<edge_weight_t,int> > Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;

int minimalTime(int t, int s, 
		int a, int b,
		vector<E> & edges,
		vector<vector<int> > & weights,
		vector<int> & hives)
{
    // build the graph and insert edges for each species
    vector<Graph> graphs;

    for(int si = 0; si < s; si++){
	graphs.push_back(Graph(edges.begin(),edges.end(),weights[si].begin(),t));
    }    
    
    vector<vector<Vertex> >parents(s);
    for(int si = 0; si < s; si++){
	parents[si] = vector<Vertex>(t);
	prim_minimum_spanning_tree(graphs[si],&(parents[si][0]));
    }

    vector<int> newWeights;
    for(size_t ei = 0; ei < edges.size(); ei++){
	E e = edges[ei];
	int minWeight = 10000;
	for(int si = 0; si < s; si++){
	    if(parents[si][e.first] == e.second || parents[si][e.second] == e.first){
		minWeight = min(weights[si][ei],minWeight);
	    }
	}
	newWeights.push_back(minWeight);
    }

    Graph gnew(edges.begin(),edges.end(),newWeights.begin(),t);
    vector<Vertex> q(num_vertices(gnew));
    vector<int> d(num_vertices(gnew));

    dijkstra_shortest_paths(gnew, vertex(a,gnew),predecessor_map(&q[0]).distance_map(&d[0]));

    return d[b];
}
