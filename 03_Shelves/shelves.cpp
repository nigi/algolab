#include <iostream>
#include <vector>

using namespace std;

struct entry {
    int shelves;
    int m;
    int n;
    bool valid;
};

int main()
{
    int N = 0; // Number of testcases
    cin >> N;

    for(int i = 0; i < N; i++)
    {
	int l,m,n; // Length of wall, length of shelf type m <= n
	cin >> l >> m >> n;
		
	int largeOnes = l/n;
	int smallOnes = (l%n)/m;
		
	int maxLength = 0;
	int maxLargeOnes = 0;
	int maxSmallOnes = 0;
		
	int initLO = largeOnes;
		
	for(; largeOnes >= 0; largeOnes--)
	{
	    smallOnes = (l - (largeOnes * n)) / m;
	    int length = largeOnes * n + smallOnes * m;
	    
	    if(length == l)
	    {
		maxLength = length;
		maxLargeOnes = largeOnes;
		maxSmallOnes = smallOnes;
		break;	
	    }else{
		if(length > maxLength)
		{
		    maxLength = length;
		    maxLargeOnes = largeOnes;
		    maxSmallOnes = smallOnes;	
		}	
	    }
			
	    if(m > 0 && largeOnes < initLO && (initLO - largeOnes) % m == 0)
	    {
		break;
	    }
    	}
	cout << maxSmallOnes << " " << maxLargeOnes  << " " << l-maxLength << "\n";
    }
	
    return 0;	
}
