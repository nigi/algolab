#include <iostream>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Min_circle_2.h>
#include <CGAL/Min_circle_2_traits_2.h>

using namespace CGAL;
using namespace std;

typedef Exact_predicates_inexact_constructions_kernel K;
typedef Min_circle_2<Min_circle_2_traits_2<K> > MinCircle;

int main(){
    std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(0);

    for(int n = 0; cin >> n && n>0;){
	vector<K::Point_2> points;
	points.reserve(n);

	for(int in = 0; in < n; in++){
	    K::Point_2 p;
	    cin >> p;
	    points.push_back(p);
	}

	MinCircle mc(points.begin(),points.end(),true);
	cout << ceil(to_double(sqrt(mc.circle().squared_radius()))) << "\n";
    }

    return 0;
}
