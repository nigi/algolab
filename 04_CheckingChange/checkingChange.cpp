#include <iostream>

using namespace std;

int main(){
	
    int n; // number of currencies or testcases
    cin >> n;
		
    for(int i = 0; i < n; i++) 
    {
	int c,m; // c.. number of coin values, m.. number of sample return
	// values
	cin >> c >> m;
	
	int coins[c];
	for(int j = 0; j < c; j++)
	{
	    cin >> coins[j];
	}
		
	int tests[m];
	int maxvalue = 0;
	for(int j = 0; j < m; j++)
	{
	    cin >> tests[j];	
	    if(tests[j] > maxvalue)
		maxvalue = tests[j];
	}
		
	//cout << "maxvalue: " << maxvalue << "\n";
		
	// Create twodimensional array for storing the data
	int coinsetValue[c][maxvalue+1];
		
	// Go through the matrix line by line
	for(int line = 0; line < c; line++)
	{
	    for(int column = 0; column < maxvalue+1; column++)
	    {
		bool avalid, bvalid;
		int a,b; // a = C(W,m-1) ... b = C(W-Sm,m)+1
		int Sm = coins[line];
				
		if(column == 0)
		{
		    coinsetValue[line][column] = 0;
		    continue;
		}
			
		if(line == 0)
		{
		    avalid = false;
		}else{
		    avalid = (coinsetValue[line-1][column] >= 0);	
		}
				
		if(column < Sm)
		{
		    bvalid = false;	
		}else{
		    bvalid = (coinsetValue[line][column-Sm] >=0);
		}
				
		if(avalid && bvalid)
		{
		    a = coinsetValue[line-1][column];
		    b = coinsetValue[line][column-Sm] + 1;
					
		    if(a < b)
		    {
			coinsetValue[line][column] = a;	
		    }else{
			coinsetValue[line][column] = b;	
		    }
					
		}else if(avalid)
		{
		    coinsetValue[line][column] = coinsetValue[line-1][column];
		}else if(bvalid)
		{
		    coinsetValue[line][column] = coinsetValue[line][column-Sm] + 1;
		}else{
		    coinsetValue[line][column] = -1;
		}
				
				
	    }	
	}
/*		
		cout << "matrix:\n";
		for(int x = 0; x < c; x++)
		{
		for( int y = 0; y < maxvalue+1; y++)
		{
		cout << coinsetValue[x][y] << "\t";	
		}	
		cout << "\n";
		}
*/		
		
	for(int t = 0; t < m; t++)
	{
	    int result = coinsetValue[c-1][tests[t]];
	    if(result < 0)
	    {
		cout << "not possible\n";	
	    }else{
		cout << result << "\n";		
	    }
	}
		
		
		
    }
	
    return 0;
}
