#include <iostream>
#include <vector>
#include <map>
#include <cassert>
#include <string>

#include <boost/graph/properties.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>

using namespace std;

typedef struct {
    string name;
    int scoredPoints;
    int scorablePoints;
} Team;

typedef pair<int,int> Matchup;

void processTestCase();
void possibleChampions(int t, int m,
		       vector<Team> & teams,
		       vector<Matchup> & matchups,
		       vector<int> & results);

int main(){
    int n; // testcases
    cin >> n;
    while(n--){
	processTestCase();
    }
}


void processTestCase(){
    int t,m; // number of teams; remaining matchups
    cin >> t >> m;
    assert(t >= 1 && t <= 100);
    assert(m >= 0 && m <= 100);

    vector<Team> teams;
    map<string,int> teamNameIndex;

    for(int it = 0; it < t; it++){
	Team team;
	team.scorablePoints = 0;
	cin >> team.scoredPoints >> team.name;
	teams.push_back(team);
	teamNameIndex[team.name] = teams.size() - 1; 
	cerr << "Name: " << team.name << "\n";
    }

    vector<Matchup> matchups;
    for(int im = 0; im < m; im++){
	Matchup match;
	string vs,team1,team2;
	cin >> team1 >> vs >> team2;
	match.first = teamNameIndex[team1];
	match.second = teamNameIndex[team2];
	cerr << "Team1: " << match.first << " Team2: " << match.second << "\n";
	teams[match.first].scorablePoints++;
	teams[match.second].scorablePoints++;
	matchups.push_back(match);
    }

    vector<int> champions;
    possibleChampions(t,m,teams,matchups,champions);

    vector<string> names;

    for(vector<int>::iterator it = champions.begin(); it != champions.end(); it++){
	names.push_back(teams[*it].name);
    }

    sort(names.begin(),names.end());
    for(vector<string>::iterator it = names.begin(); it != names.end(); it++){
	cout << *it << " ";
    }


    cout << "\n";
}

using namespace boost;

typedef pair<int,int> E;
typedef adjacency_list_traits <vecS,vecS,directedS> Traits;
typedef adjacency_list<vecS,vecS,directedS,
		       no_property,
		       property<edge_capacity_t, long,
				property<edge_residual_capacity_t, long,
					 property<edge_reverse_t, Traits::edge_descriptor> > > >  Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::edge_iterator EIterator;

typedef property_map<Graph,edge_capacity_t>::type CapacityMap;
typedef property_map<Graph,edge_residual_capacity_t>::type ResidualMap;
typedef property_map<Graph,edge_reverse_t>::type ReverseMap;

void possibleChampions(int t, int m,
		       vector<Team> & teams,
		       vector<Matchup> & matchups,
		       vector<int> & results)
{
    // Graph: t + m + 2 vertices
    // Edges: - from source to each match with capacity one
    //        - from source to each team with capacity of its starting points
    //        - from each match to its two teams with capacity one
    //        - from each team to the target with capacity of the current team

    // Create the graph
    Graph g(t+m+2);

    // Extract the property maps
    CapacityMap capacities = get(edge_capacity,g); // the edge capacities
    ReverseMap reverseEdges = get(edge_reverse,g); // map from edge (u,v) to (v,u)
    ResidualMap residuals = get(edge_residual_capacity,g); // the capacity residuals
    
    Vertex source(t+m);
    Vertex target(t+m+1);

    //  vector<long> capacities;
    bool s;

    long totalPoints = 0;
    
    for(size_t it = 0; it < teams.size(); it++){
	Edge e,erev;
	tie(e,s) = add_edge(source,it,g);
	tie(erev,s) = add_edge(it,source,g);

	reverseEdges[e] = erev;
	reverseEdges[erev] = e;

	capacities[e] = teams[it].scoredPoints;
	capacities[erev] = 0;

	totalPoints += teams[it].scoredPoints;
    }

    for(size_t im = 0; im < matchups.size(); im++){
	Matchup m = matchups[im];
	int matchIndex = t + im;

	totalPoints++;

	Edge e_source_match, e_match_team1, e_match_team2;
	Edge e_source_match_rev, e_match_team1_rev,e_match_team2_rev;

	// Add the edges and their reverse edges
	tie(e_source_match,s) = add_edge(source,matchIndex,g);
	tie(e_source_match_rev,s) = add_edge(matchIndex,source,g);
		
	tie(e_match_team1,s) = add_edge(matchIndex,m.first,g);
	tie(e_match_team1_rev,s) = add_edge(m.first,matchIndex,g);

	tie(e_match_team2,s) = add_edge(matchIndex,m.second,g);
	tie(e_match_team2_rev,s) = add_edge(m.second,matchIndex,g);	

	// Define their capacities
	capacities[e_source_match] = 1;
	capacities[e_match_team1] = 1;
	capacities[e_match_team2] = 1;

        // Define their capacities
	capacities[e_source_match_rev] = 0;
	capacities[e_match_team1_rev] = 0;
	capacities[e_match_team2_rev] = 0;

	// define the mappings to reverse edges
	reverseEdges[e_source_match] = e_source_match_rev;
	reverseEdges[e_source_match_rev] = e_source_match;

	reverseEdges[e_match_team1] = e_match_team1_rev;
	reverseEdges[e_match_team1_rev] = e_match_team1;
	
	reverseEdges[e_match_team2] = e_match_team2_rev;
	reverseEdges[e_match_team2_rev] = e_match_team2;
    }

    vector<Edge> teamEdges;

    // add edges from teams to the sink. storing the edges allows
    // for later modification of the capacities
    for(size_t it = 0; it < teams.size(); it++){
	Edge e_team_target, e_team_target_rev;
	
	// Add edge and reverse edge
	tie(e_team_target,s) = add_edge(it,target,g);
	tie(e_team_target_rev,s) = add_edge(target,it,g);
	
	// Set the reverse edges
	reverseEdges[e_team_target] = e_team_target_rev;
	reverseEdges[e_team_target_rev] = e_team_target;

	teamEdges.push_back(e_team_target);
    }

    // check for each team if it can be champion
    for(int it = 0; it < teams.size(); it++){
	// Compute the teams maximum scorable points
	long maxPoints = teams[it].scoredPoints + teams[it].scorablePoints;
	
	for(size_t jt = 0; jt < teamEdges.size(); jt++){
	    capacities[teamEdges[jt]] = maxPoints;
	}

	long maxflow = edmonds_karp_max_flow(g,Vertex(source),Vertex(target));
	if(maxflow == totalPoints){
	    results.push_back(it);
	}

	cerr << "Maxflow: " << maxflow << " TotalPoints: " << totalPoints << "\n";
    }
}
