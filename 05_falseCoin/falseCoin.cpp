#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

#include <cassert>

using namespace std;

enum CoinState {
    NONE, // Nothing is known about this coin
    GOOD,  // The coin has to be good
    GREATER,// The coin has been seen in at least one larger set
    LESSER // The coin has been seen in at least one smaller set
};

// Forward definitions
void processDataSet();
void processMeasurement(char outcome, vector<int> & left, vector<int> & right, // inputs
			int & falseCoin, int & unknowns, vector<CoinState> & states); // outputs);

int main()
{
    int M; // Number of datasets
    cin >> M; // Read input
    
    int m = 0;
    while(m++ < M)
    {
        // Per Dataset:	
	processDataSet();
    }
}

void processDataSet(){

    // Per Dataset:	
    // Read data
    int N; // number of coins (1 <= N <= 100)
    int K; // number of weighings (1 <= K <= 100)
   
    cin >> N >> K; 
    // cout << "N:" << N << " K:" << K << "\n";
    // For each coin save a state which denotes where the coin has been seen
    vector<CoinState> states(N);
    int unknowns = N;
    int falseCoin = -1;

    while(K-- > 0)
    {
	// Read measurement data and process it
	int Pi; // 1 <= Pi <= N/2 ... Number of coins in each pan
	vector<int> leftCoins;
	vector<int> rightCoins;
	char c; // outcome

	cin >> Pi; 

	int p = 0;
	while(p++ < Pi)
	{
	    int tmp;
	    cin >> tmp;
	    leftCoins.push_back(tmp-1);
	}
	
	p = Pi;

	while(p++ < 2*Pi)
	{
	    int tmp;
	    cin >> tmp;
	    rightCoins.push_back(tmp-1);
	}
	
	cin >> c;

	processMeasurement(c, leftCoins, rightCoins, falseCoin, unknowns, states);
	// cout << "Left unknowns: " << unknowns << "\n";

    }

    if(unknowns == 1){
	for(int i = 0; i < N; i++){
	    if(states[i] != GOOD) {falseCoin = i; break;}
	}
    }

    if(falseCoin == -1){
	// cannot decide on falseCoin
	cout << "0\n";
    }else{
	cout << falseCoin+1 << "\n";
    }
}

void setAll(CoinState s,vector<int> & coins, vector<CoinState> & states, int & unknowns){
    for(int i = 0; i<coins.size(); i++){
	if(s == GOOD && states[coins[i]] != GOOD) unknowns--;
	states[coins[i]] = s;
    }
}

void checkAll(CoinState check, CoinState set, vector<int> & coins, vector<CoinState> & states,
	      int & unknownsSub, int & lastUnknown, int & unknowns){
    // Process the left part
    for(int i = 0; i < coins.size(); i++){
	CoinState s = states[coins[i]];
	if(s == GOOD) {unknownsSub--;continue;}
	if(s == check) {states[coins[i]] = GOOD; unknownsSub--; unknowns--;continue;}
	states[coins[i]] = set;
	lastUnknown = coins[i];
    }
}


void processMeasurement(char outcome, vector<int> & left, vector<int> & right, // inputs
			int & falseCoin, int & unknowns, vector<CoinState> & states) // outputs
{
    assert(right.size() == left.size());
    assert(right.size() > 0);
    assert(states.size() > 1);

    if(!( falseCoin < 0 && unknowns > 1)) return;

    int unknownsSub = left.size() + right.size();
    int lastUnknown = -1;

    switch(outcome)
    {
    case '=':
    {
	setAll(GOOD,left,states,unknowns);   
	setAll(GOOD,right,states,unknowns);
    }break;

    case '<':
    {
	// Process the left and right part
	checkAll(GREATER,LESSER,left,states,unknownsSub,lastUnknown,unknowns);
	checkAll(LESSER,GREATER,right,states,unknownsSub,lastUnknown,unknowns);

	if (unknownsSub == 1) falseCoin = lastUnknown;

	assert(unknownsSub > 0);

    }break;
    case '>':{
	// Process the left and right part
	checkAll(GREATER,LESSER,right,states,unknownsSub,lastUnknown,unknowns);
	checkAll(LESSER,GREATER,left,states,unknownsSub,lastUnknown,unknowns);

	if (unknownsSub == 1) falseCoin = lastUnknown;

	assert(unknownsSub > 0);
    }break;
		
    default:{
	assert(false);
    }break;
    
    }
}
