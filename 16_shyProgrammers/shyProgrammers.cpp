#include <iostream>
#include <vector>
#include <cassert>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/boyer_myrvold_planar_test.hpp>

using namespace std;

typedef pair<int,int> E;

void processTestCase();
bool placementPossible(int n, int m, vector<E> & friends);

int main(){
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);

    int t; // testcases
    cin >> t;
    assert(t >= 1 && t <= 100);
    while(t--){
	processTestCase();
    }
}

void processTestCase(){
    int n,m; // num of employees, num of pairs of friends
    cin >> n >> m;
    assert(n >= 1 && n <= 100000);
    assert(m >= 0 && m <= 200000);
    
    vector<E> friends;

    for(int im = 0; im < m; im++){
	E f;
	cin >> f.first >> f.second;
	assert(f.first >= 0 && f.first <= n);
	assert(f.second >= 0 && f.second <= n);
	assert(f.first < f.second);
	friends.push_back(f);
    }

    bool result = placementPossible(n,m,friends);    
    
    cout << (result ? "yes" : "no") << "\n";
}

using namespace boost;
typedef adjacency_list<vecS,vecS,undirectedS> Graph;

bool placementPossible(int n, int m, vector<E> & friends)
{
    for(int in = 0; in < n; in++)
    {
	E e(in,n); // edge from each person to the outside
	friends.push_back(e);
    }

    Graph g(friends.begin(),friends.end(),n+1);

    return boyer_myrvold_planarity_test(g);
}
