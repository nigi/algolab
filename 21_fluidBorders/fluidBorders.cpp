#include <iostream>

using namespaces std;

void processTestCase();

int main(){
    int n;
    cin >> n;
    while(n--){
	processTestCase();
    }
    return 0;
}

struct VotingResult {
    vector<int> order;
};

void processTestCase(){
    int m; // m lines describing the election results
    cin >> m;
    
    vector<VotingResult> results;

    for(int im = 0; im < m; im++){
	VotingResult vr;
	for(int jm = 0; jm < m-1; jm++){
	    int tmp; cin >> tmp;
	    vr.order.push_back(tmp);
	}
	results.push_back(vr);
    }
    
    
}
