
#include <iostream>
#include <string>
#include <vector>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

using namespace std;

bool processTestCase();

int main()
{
    while(true){
	if(!processTestCase()) break;
    }
}

bool processTestCase(){
    long n;
    if(!(cin>>n) || n == 0) return false;
    K::Ray_2 r;
    cin >> r;

    bool hitfound = false;

    for(long in = 0; in < n; in++){
	K::Segment_2 seg;
	cin >> seg;
	if(hitfound) continue;
	if(CGAL::do_intersect(seg,r)) hitfound = true;
    }

    cout << (hitfound ? "yes" : "no") << "\n";
    return true;
}
