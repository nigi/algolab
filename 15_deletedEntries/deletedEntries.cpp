#include <iostream>
#include <cassert>
#include <vector>
#include <utility>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/connected_components.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS,vecS,undirectedS > Graph;
typedef graph_traits<Graph>::vertex_descriptor Vertex;
typedef graph_traits<Graph>::edge_descriptor Edge;

typedef pair<int,int> E;

typedef struct {
    vector<int> groupA;
    vector<int> groupB;
    vector<int> groupC;
} Division;

void processTestCase();
bool hasValidDivision(int n, int m, 
		      vector<E> & edges,
		      Division & result);

// Depth first search visitor class for coloring the vertices
class three_color_visitor : public default_dfs_visitor {
private:
    Division & division;
    int depth;
public:
    three_color_visitor(Division & div) : division(div), depth(0){};

    template<typename Vertex, typename Graph>
    void discover_vertex(Vertex u, Graph g){
	switch(depth % 3){
	case 0:
	    division.groupA.push_back(u);
	    break;
	case 1:
	    division.groupB.push_back(u);
	    break;
	case 2:
	    division.groupC.push_back(u);
	    break;
	}

	depth++;
    }

    template<typename Vertex, typename Graph>
    void finish_vertex(Vertex u, Graph g){
	depth--;
    }
};

int main(){
    // make IO more efficient
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);

    int t; // number of testcases
    cin >> t;
    assert(t >= 1 && t <= 100);
    while(t--){
	processTestCase();
    }
}

void processTestCase(){
    // n ... number of students
    // m ... number of pairs of students that have each others contact details
    int n,m;
    cin >> n >> m;
    assert(n >= 1 && n <= 1000000);
    assert(m >= 1 && m <= 2000000);

    vector<E> edges;
    for(int ei = 0; ei < m; ei++){
	E etmp;
	cin >> etmp.first >> etmp.second;
	assert(etmp.first >= 0 && etmp.first < n);
	assert(etmp.second >= 0 && etmp.second < n);
	edges.push_back(etmp);
    }

    Division result;
    
    if(hasValidDivision(n, m, edges,result)){
	cout << "yes\n";
	cout << result.groupA.size();
	for(int i = 0; i < result.groupA.size(); i++){
	    cout << " " << result.groupA[i];
	}
	cout << "\n" << result.groupB.size();
	for(int i = 0; i < result.groupB.size(); i++){
	    cout << " " << result.groupB[i];
	}
	cout << "\n" << result.groupC.size();
	for(int i = 0; i < result.groupC.size(); i++){
	    cout << " " << result.groupC[i];
	}
	cout <<"\n";
    }else{
	cout << "no\n";
    }
}

bool hasValidDivision(int n, int m, 
		      vector<E> & edges,
		      Division & result)
{
    // Check for bordercases
    if(n < 3) return false;

    Graph g(edges.begin(), edges.end(),n);

    vector<int> c(num_vertices(g));
    int components = connected_components(g, make_iterator_property_map(c.begin(),get(vertex_index,g)));
    if(components > 1) return false;

    three_color_visitor vis(result);
    depth_first_search(g, visitor(vis));

    return true;
}
