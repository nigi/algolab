#include <iostream>
#include <utility>
#include <vector>
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/prim_minimum_spanning_tree.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

using namespace std;
using namespace boost;

typedef adjacency_list<vecS,vecS,undirectedS,no_property,property<edge_weight_t,int> > Graph;
typedef graph_traits<Graph> Traits;
typedef Traits::vertex_descriptor Vertex;
typedef Traits::edge_descriptor Edge;

void processTestCase();

int main(){
    int t; // number of testcases
    cin >> t;
    while(t--){

	processTestCase();
    }

}

void processTestCase()
{
    int n,m; // vertices, edges
    cin >> n >> m;
    
    typedef pair<int,int> E;
    vector<E> myedges;
    vector<int> myweights;

    while(m--){
	int v1,v2,w;
	cin >> v1 >> v2 >> w;
	E e1(v1,v2);
	myedges.push_back(e1);
	myweights.push_back(w);
    }

    Graph g(myedges.begin(), myedges.end(), myweights.begin(), n);
    
    property_map<Graph, edge_weight_t>::type EdgeWeightMap = get(edge_weight_t(), g);


    vector<Vertex> p(num_vertices(g));

    // Compute the minimum spanning tree
    prim_minimum_spanning_tree(g,&p[0]);

    int weightSum = 0;
    for(int i = 0; i < p.size(); i++){
	if(p[i] != i)
	    weightSum += get(EdgeWeightMap,edge(i,p[i],g).first);
    }

    // Compute the shortest paths with dijkstra
    vector<Vertex> q(num_vertices(g));
    vector<int> d(num_vertices(g));

    Vertex s = vertex(0,g);

    dijkstra_shortest_paths(g, s,predecessor_map(&q[0]).distance_map(&d[0]));
    int maxd = 0;
    for(int i = 0; i < d.size(); i++){
	maxd = d[i] > maxd ? d[i] : maxd;
    }

    cout << weightSum << " " << maxd << endl;
}
