#include <iostream>
#include <cassert>
#include <vector>
#include <string>
#include <map>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

using namespace std;

struct Student {
    vector<string> characteristics;
};

void processTestCase();
bool hasOptimalMatching(long n, long c, long f,
			vector<Student> & students,
			map<string,vector<long> > & charToStudent);



int main(){
    cin.sync_with_stdio(false);
    cout.sync_with_stdio(false);
    long t;
    cin >> t;
    while(t--){
	processTestCase();
    }
    return 0;
}

void processTestCase(){
    long n,c,f; // number of students; number of characteristics; minimum number of common characteristics in Dr. Fuzzbrains solution
    cin >> n >> c >> f;
    /*
    assert(n >= 2 && n <= 400);
    assert(n % 2 == 0);
    assert(c <= 10);
    assert(f >= 1);
    */
    vector<Student> students;
    map<string,vector<long> > charToStudent;

    for(long in = 0; in < n; in++){
	Student s;
	for(long ic = 0; ic < c; ic++){
	    string characteristic;
	    cin >> characteristic;
	    s.characteristics.push_back(characteristic);
	    charToStudent[characteristic].push_back(in);
	}
	students.push_back(s);
    }

    if(hasOptimalMatching(n,c,f,students,charToStudent)){
	cout << "not optimal\n";
    }else{
	cout << "optimal\n";
    }
}

using namespace boost;

typedef adjacency_list<vecS,vecS,undirectedS,property<vertex_index_t,long> > Graph;
typedef graph_traits<Graph>::edge_descriptor Edge;
typedef graph_traits<Graph>::vertex_descriptor Vertex;

/*
void dumpMap(map<string,vector<long> > & charToStudent){
    for(std::map<string,vector<long> >::iterator it = charToStudent.begin(); it != charToStudent.end(); it++){
	cerr << it->first << " ";
	for(long i = 0; i < it->second.size(); i++){
	    cerr << it->second[i] << " ";
	}
	cerr << "\n";
    }
}

void dumpEdges(Graph &g){
    graph_traits<Graph>::edge_iterator first, last;
    cerr << "Edges: \n"; 
    for( tie(first,last) = edges(g); first != last; ++first){
	cerr << source(*first,g) << " " << target(*first,g) << "\n";
    }
}
*/
bool hasOptimalMatching(long n, long c, long f,
		       vector<Student> & students,
			map<string,vector<long> > & charToStudent)
{
    
    map<Edge, long> parEdges;

    Graph g(n);
    
    for(long is = 0; is < students.size(); is++){
	Student s = students[is];
	for(long ic = 0; ic < s.characteristics.size(); ic++){
	    string charact = s.characteristics[ic];
	    vector<long> & buddies = charToStudent[charact];
	    
	    for(long ibuddy = 0; ibuddy < buddies.size(); ibuddy++){
		long buddy = buddies[ibuddy];
		if(buddy == -1) continue;
		if(buddy == is) { buddies[ibuddy] = -1; continue;}

		Edge e; bool success;
		tie(e,success) = edge(is,buddy,g);
		
		if(success){
		    // If edge is already available then just increase its count
		    parEdges[e]++;
		}else{
		    // Otherwise add the edge
		    tie(e,success) = add_edge(is,buddy,g);
		    parEdges[e] = 1;
		    
		}	
	    }	    
	}
    }
    

    vector<Edge> removeEdges;

    for(std::map<Edge,long>::iterator iter = parEdges.begin(); iter != parEdges.end(); ++iter)
    {
	Edge e = iter->first;
	if(iter->second <= f){
	    removeEdges.push_back(e);
	}
    }

    for(long i = 0; i < removeEdges.size(); i++){
	remove_edge(removeEdges[i],g);
    }

    //dumpEdges(g);
    vector<Vertex> indexMap(n,0);

    //dumpMap(charToStudent);

    // Compute the maximum cardinality matching
    edmonds_maximum_cardinality_matching(g,&indexMap[0]);

    const Vertex NULL_VERTEX = graph_traits<Graph>::null_vertex(); 

    for(long i = 0; i < n; ++i){
	if(indexMap[i] == NULL_VERTEX)
	    return false;
	    }

    return true;
}
